#Defino una variable que usare mas adelante en el script
VAR1="Aguarde unos segundos..."

#Uso de variables ya definidas en el sistema
echo "Bienvenido $USER"
echo "Usted esta trabajndo sobre la maquina virtual $HOSTNAME"


#Uso de variables globales configuradas en bashrc y profile, aplicado a todos los usuarios
echo "El script en cuestion fue solicitado por el insituto: $FACULTAD"


echo "$VAR1"
sleep 3

# Desde otro screipt tomamos las variables exportadas
source TAREA010328-CONSULTA.sh



#Uso de Variables exportadas

echo El dia de hoy es "$d"
echo $VAR2


#EOF

