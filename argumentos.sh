#!/bin/bash
# argumentos.sh <hostip> <pings> 

hostip=$1
pings=$2

# -4 IPv4
# -6 IPv6
# -b broadcast 
# -c cantindad de pings

# verficar si la variable '$protocolo' tiene contenido.

test -z $protocolo && exit
# Testear que sea un 4 o un 6

test $protocolo -eq 4 -o $protocolo -eq 6 || exit

exit

ping -c $pings $protocolo $hostip > /dev/null 2>&1 && echo "Se alcanzo el destino $hostip" || echo "No se pudo alcanzar el desitno $hostip"

