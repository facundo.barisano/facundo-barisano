#!/bin/bash

#El script debera de ejecutarse como permisos de root si se desea ver ficheros que esten en todo el sistema
echo ""
echo "Actualmente se encuentra logueado con el usuario $USER"
echo ""
echo "Si desea buscar un fichero en todo el sistema, asegurese de que el usuario $USER cuente con permisos de root, de lo contrario solo podra buscar ficheros donde dicho usuario tenga acceso"
echo ""

read -p "Indicar la ruta completa del fichero a buscar:" FILE
if [ -e "$FILE" ]; then
	echo "El archivo $FILE existe en el sistema"
else
	echo "El archivo $FILE no existe en el sistema"
	#Salgo del programa con un 'exit status 1 ya que el argumento (el archivo) no esta en el sistema'
	exit 1
fi

#Verificamos permisos de ejecucion
if [ -x "$FILE" ]; then
	echo "Este archivo es ejecutable"
	ls -l $FILE
	read -p "Quiere ejecutar el archivo? [Y/N]:" OPCION
		if [[ $OPCION =~ [Yy] ]]; then
			echo "Ejecutando archivo $FILE..."
		fi
	else
		#Salgo del programa con un 'exit status 1' si no se entrego como argumento Yy
		exit 1
	
	#Si no tene permisos de ejecucion pero somos dueños, pregunto si le quiero dar permisos de ejecucion
	test -O $FILE
	if [ $? -eq 0 ]; then
		read -p "Quiere que le otorgue permisos de ejecucion al archivo $FILE [Y/N]" OPCION2
		if [[ $OPCION2 =~ [Yy] ]]; then
			chmod +x $FILE
			echo "Se le otorgaron permisos de ejecucion al archivo $FILE"
			ls -l $FILE
		else
			echo "No se otorgara permisos"
			#Salgo del programa con un 'exit status 1' si no se entrego comoa rgumento Yy
			exit 1
		fi
	else
		echo "Usted no es dueño del archivo $FILE"

	fi
fi

