#!/bin/bash

#!/bin/bash

if [ $UID -ne 0 ]; then
        echo "Ejecute este programa como root o:"
        echo "sudo $0"
        exit 1
fi

# 'is-active' devuelve 'exit status 0' si el servicio se encuentra activo
# necesitamos que la condicion devuelva 'true' si 'is-active' devuelve
# 'exit status' distito de '0'
# '!' invierte el sentido de la condicion
if ! systemctl is-active --quiet $1;then

        systemctl status $1
        read -p "El servicio esta detenido, desea inciarlo? Y/N: " ans

        # anidacion de if
        # '=~ [Yy]' expresion regular igual que 'Y' o 'y'
        if [[ $ans =~ [Yy] ]];then
                systemctl start $1
                # se sale del programa y se estable el 'exit status' en '0'
                systemctl is-active $1
                exit 0
        else
                echo "no se hace nada"
                # se sale del programa
                # y se establece el 'exit status' del script en '1'
                exit 1
        fi
else
        systemctl is-active $1
fi
