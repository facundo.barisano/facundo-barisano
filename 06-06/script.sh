#!/bin/bash

args=( $@ )

# Declaro la funcion help para la guia de ayuda
Help(){
	cat Guia.txt
	exit 1
}


# Declaro la funcion para la validacion de la IP o hostname

ValidacionIP-Hostname(){
if [[ ${args[-1]} =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
# Si el if anterior pasa, se separa la direccion IP en cuatro partes segun la ubicacion de los "."
# Y lo ubica en diferentes partes del array

       	OIFS=$IFS #Aca defino una variable que sirva de backup para el valor actual de IFS
       	IFS='.' #Aca seteo la variable IFS para que bash analice la IP por cada octeto
       	ip=(${args[-1]})  #Aca establezco la variable ip para despues validar que sea menor a 255
       	IFS=$OIFS #Recupera el $IFS original	
	
	if  [[ ${ip[0]} -le 255 ]] && [[ ${ip[1]} -le 255 ]] && [[ ${ip[2]} -le 255 ]] && [[ ${ip[3]} -le 255 ]]; then
		       	echo ""
               		echo "La IP es valida, se procedera con el ping siempre y cuando las opciones pasadas sean validas."
                	echo ""
			
	else
                	echo ""
                	echo "La IP ingresada no es valida. Lea la guia de ayuda para mas informacion"
                	echo ""
                	Help
       	fi


#En caso de agregar un hostname o dominio, valido que el mismo sea correcto segun los requisitos del rfc952

else
       if [[ ${args[-1]} =~ ^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$ ]]; then

                echo ""
                echo "El hostname es valido, se procedera con el ping siempre y cuando las opciones pasadas sean validas"
                echo ""

        else
                echo ""
                echo "La IP o el hostname es invalido. Lea la guia de ayuda para mas informacion"
                echo ""
                Help
                exit 1
        fi
fi
}


# Declaro la funcion para verificar que sea un numero entero y positivo
NumeroEntero(){
         if [[ $cantidad =~ ^[1-9]+[0-9]*$ ]]; then
                                counter="-c $cantidad"
                        else
                                echo "Error en la opcion '-C'. Debe ingresar un valor positivo y entero."
                                exit 1
                        fi
                }

# Declaro la funcion para validar el protocolo
Protocolo(){
         if [ $proto -eq 4 -o $proto -eq 6 ]; then
                                p="-$proto"

                        else
                                echo "Error en la opcion '-p'. Debe ingresar un valor igual a 4 o 6."
                                exit 1
                        fi
                }



		
# Aca arrancaria la validacion del script tomando a las funciones ya declaradas
# Valido que se pase por lo menos un parametro
if [ "$#" -gt "1" ] ; then
	ValidacionIP-Hostname

for opcion in "${!args[@]}"; do 

	case ${args[$opcion]} in 
		-C) cantidad=${args[$(($opcion+1))]} 
			NumeroEntero

			;;

		-T) timestamp="-D"

			;;

		-p) proto=${args[$(($opcion+1))]}
			Protocolo
			
			;;

		-b) b="-b"

			;;

		esac
	

	done

sleep 1
ping $b $timestamp $p $counter ${args[-1]} #el hostname es el unico valor del array
exit 0

else
        echo ""
        echo "Ingrese por lo menos una opcion y un valor para que el script arroje resultados"
        echo ""
        exit 1
fi

