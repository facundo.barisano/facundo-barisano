#!/bin/bash


# ejecutamos como condicion el programa 'grep'
# cuyo argumento se pasa como argumento del script
# si el 'exit status' de 'grep' es '0' entocnes la condicion es 'true'
# si el 'exit status' de 'grep' es '!0' entonces la condicion es 'false'
# cuando evualamos la salida de un comando no hace falta corchetes
# se utilizan corchetes cuando hacemos comparaciones.

# evaluamos la existencia del usuario en el fichero 'passwd'
# almacenamos el resultado en una variable.
if  user=$(grep -w "$1" /etc/passwd) ;then
        # condicion 'true'
        echo "Econtrado el usario $1"
else
        # condicion 'false'
        echo "No se encuentra el usuario"
        # Al no existir el usuario de entrada
        # se sale del programa
        exit
fi


# Evaluar si el usuario ingresado corresponde a:
#        - Usuario 'root' (id 0)
#        - Usuario especial del sistema (id 1~499)
#        - Usuario estandar (id +500)

# Array con los niveles de usuario
usrlvl=( "root" "especial sistema" "usuario estandar" )

# aislar la 'id' de usuario
user=$(echo $user | cut -d ":" -f 3)

if [ $user -eq 0 ]; then
        echo "el usuario es ${usrlvl[0]}"
# Evlaumos un rango entre 1 y 499
# ambas comparaciones deben devovler 'true' para
# que la condicion sea 'true'
elif [ $user -le 499 ] && [ $user -ge 1 ];then
        echo "el usuario es ${usrlvl[1]}"
else
        echo "el usuario es ${usrlvl[2]}"
fi
