#!/bin/bash


# ejecutamos como condicion el programa 'grep'
# cuyo argumento se pasa como argumento del script
# si el 'exit status' de 'grep' es '0' entocnes la condicion es 'true'
# si el 'exit status' de 'grep' es '!0' entonces la condicion es 'false'
# cuando evualamos la salida de un comando no hace falta corchetes
# se utilizan corchetes cuando hacemos comparaciones.

if  grep "$1" /etc/passwd >> /dev/null ;then
        # condicion 'true'
        echo "Los siguientes resultados segun '$1'"
        grep $1 /etc/passwd
else
        # condicion 'false'
        echo "Ningun resultado"
fi
