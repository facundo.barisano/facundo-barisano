#!/bin/bash
timestamp=$(date +%Y-%m-%d--%H.%M.%S)
Backup_File=$1
Dest=/$HOME/backup/
Temp=/tmp/$USER/
filename1=backupdiario-${Backup_File//\//_}-${timestamp}.tar.gz
filename2=backupsemanal-${Backup_File//\//_}-${timestamp}.tar.gz
filename3=backupmensual-${Backup_File//\//_}-${timestamp}.tar.gz


echo "Script para realizar backup de ficheros usando rsync"

# se levanta el repositorio del backup y el tmp donde se va a sincronizar luego
mkdir -p $Dest && mkdir -p $Temp

read -p "Indicar la ruta completa del fichero que desea tomar backup : " Backup_File

tar -cvpzf ${Dest} diario/${filename1} ${Backup_File}
find $Dest diario/$filename1/* -mtime +7 -delete

tar -cvpzf ${Dest} semanal/${filename2} ${Backup_File}
find $Dest semanal/$filename2/* -mtime +31 -delete

tar -cvpzf ${Dest} mensual/${filename3} ${Backup_File}
find $Dest mensual/$filename3/* -mtime +365 -delete


# Verificamos con llave publica
if [ ! -f $HOME/.ssh/id_rsa.pub ]; then
	echo "A continuacion se generara su llave SSH. Por favor presione 3 veces enter (blank)"
       ssh-keygen -t rsa
	echo "Se solicitara enviar la llave al host remoto. Escriba 'yes' para continuar conectado"
	sleep 1
       ssh-copy-id -i ~/.ssh/id_rsa.pub $USER@localhost
fi

# Realizamos la sincraonizacion del fichero /tmp/$USER/ con el /$HOME/backup/
rsync -a $TEMP $USER@localhost:$Dest


# Uso de cron
#echo "0 10 * * * /home/data_backup.sh" /home/data >/dev/null 2>&1
#15 0 * * * /home/


