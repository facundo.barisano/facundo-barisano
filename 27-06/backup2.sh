#!/bin/bash
timestamp=$(date +%Y-%m-%d--%H.%M.%S)
Backup_File=$1
Dest=/$HOME/backup/
Temp=/tmp/$USER/
filename=backup-${Backup_File//\//_}-${timestamp}.tar.gz

echo ""
echo "Bienvenido!"
echo "La funcionaldiad de este script es para realizar backup de ficheros hacia un sitio remoto."
echo "Una vez ejecutado el script el backup se tomara todos los dias a las 02:00hs"
echo "Por favor, siga las instrucciones que se le presentaran a continuacion para llevar a cabo el backup."

# se levanta el repositorio del backup y el tmp donde se va a sincronizar luego
mkdir -p $Dest && mkdir -p $Temp

read -p "Indicar la ruta completa del fichero que desea tomar backup : " Backup_File

if [ -e "$Backup_File" ]; then
	tar -cvpzf ${Dest}/${filename} ${Backup_File}
	echo ""
	echo "Podra verificar la ubicacion del backup del fichero en la ruta local $Dest"
	echo ""
else
	echo "El fichero $Backup_File no existe en el sistema"
	exit 1
fi



# Verificamos con llave publica
if [ ! -f $HOME/.ssh/id_rsa.pub ]; then
	echo ""
	echo "A continuacion se generara su llave SSH para realizar el backup remoto. Por favor presione 3 veces enter (blank)"
       ssh-keygen -t rsa
	echo ""
        echo "Se solicitara enviar la llave al host remoto. Escriba 'yes' para continuar conectado"
	sleep 1
       ssh-copy-id -i ~/.ssh/id_rsa.pub $USER@localhost       
fi

# Realizamos la sincraonizacion del fichero /$HOME/backup/ hacia /tmp/$USER/
# autenticando via llave, no pedira contraseña.

echo "Se sincronizara el backup realizado en $Dest hacia la ubicacion remota en $USER@localhsot:$Temp"

rsync -a $Dest $USER@localhost:$Temp


# automatizo la tarea con servicio cron a las 02hs

(crontab -l ; echo "00 02 * * * /home/istea/facundo-barisano/27-06/backup2.sh") | sort - | uniq - | crontab -
echo ""
echo "Se programo el job de backup para que se ejecute todos los dias a las 02:00hs."
crontab -l
echo ""
echo "Gracias por su atencion."

#con crontab -l listo los crontab jobs, luego con el echo hago un print del nuevo comando que se ejectuara a las 02hs
#con cronbtab - añado el print que genere previamente haciendo previamente un sort para ordenar los elementos alfabetica y
#numericamente y con el uniq elimino la lineas que este duplicadas en esa lista
