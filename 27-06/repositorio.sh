#!/bin/bash

echo "Este script permite Activar o Desactivar los distintos repositorios para la distribucion"
echo "Los repositorios son 'main', 'universe', 'multiverse', 'restricted'"
echo "Tambien podra procesar ficheros '.deb' y resolver las dependencias"
echo ""
echo "A continuacion se detallara las opciones del script:"

opc=0

while [ $opc -ne 11 ] ; do

echo "1. Verifica que repositorios estan habilitados"
echo "2. Habilita repositorios main."
echo "3. Habilita repositorios universe."
echo "4. Habilita repositorios multiverse."
echo "5. Habilita repositorios restricted."
echo "6. Deshabilita repositorios main."
echo "7. Deshabilita repositorios universe"
echo "8. Deshabilita repositorios multiverse."
echo "9. Deshabilita repositorios restricted."
echo "10. Descargar programa (se le consultara si desea instalarlo)"
echo "11. Salir"
echo
read -p "Que opcion desea utilizar :" opc



case $opc in

	1)echo "Los repositorios habilitados se listaran a continuacion: "
		echo
		grep ^deb /etc/apt/sources.list
		echo
		sleep 1
		;;

	2)echo "Se habilitaran los repositorios 'main'."
		sudo add-apt-repository main	
		sleep 1
		;;

	3)echo "Se habilitaran los repositorios 'unvierse'."
		sudo add-apt-repository universe
                sleep 1
                ;;

	4)echo "Se habilitaran los repositorios 'multiverse'"
		sudo add-apt-repository multiverse
                sleep 1
                ;;

	5)echo "Se habilitaran los repositorios 'restricted'"
		sudo add-apt-repository restricted
                sleep 1
                ;;

	6)echo "Se deshabilitaran los repositorios 'main'."
		sudo add-apt-repository --remove main
		sleep 1
		;;

	7)echo "Se dehabilitaran los repositorios 'universe'."
		sudo add-apt-repository --remove universe
                sleep 1
                ;;

        8)echo "Se deshabilitaran los repositorios 'multiverse'."
                sudo add-apt-repository --remove multiverse
                sleep 1
                ;;

        9)echo "Se deshabilitaran los repositorios 'restricted'"
                sudo add-apt-repository --remove restricted
                sleep 1
                ;;

	10)read -p "Como primera instancia se le solicitara que ingrese el programa a instalar/descargar :" programa
		if [ -f "/usr/bin/$programa" ]; then
			echo "Este programa ya esta instalado"
			exit 1
		else
			sudo apt install -d $programa
			echo "Se realizo la descarga del programa $programa"
			echo ""
			read -p "Desea instalar el programa? [s/n]" rta
                		if [[ $rta =~ [sS] ]]; then
                        		sudo dpkg -i /var/cache/apt/archives/*.deb
                        		sudo apt-get install -f
                       			echo "Se realizo la instalacion del programa '$programa' y se resolvieron sus dependencias"
				else
                        		echo "No se instalara el programa '$programa'"
				fi
		fi
		;;
	
	
	11)echo "Usted decidio salir del script"
                sleep 1
                ;;


	*)clear
		echo $opc no es una ocion valida
		exit 1
		;;

esac


done

